package ru.t1.aayakovlev.tm;

import ru.t1.aayakovlev.tm.constant.ArgumentConstant;
import ru.t1.aayakovlev.tm.constant.CommandConstant;
import ru.t1.aayakovlev.tm.util.FormatUtil;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        processArgument(args);

        processCommand();
    }

    private static void processCommand() {
        System.out.println("**Welcome to Task Manager!**");
        Scanner scanner = new Scanner(System.in);
        String command;

        while(!Thread.currentThread().isInterrupted()) {
            System.out.println("Enter command:");
            command = scanner.nextLine();

            switch (command) {
                case CommandConstant.ABOUT:
                    showAbout();
                    break;
                case CommandConstant.VERSION:
                    showVersion();
                    break;
                case CommandConstant.HELP:
                    showCommandHelp();
                    break;
                case CommandConstant.INFO:
                    showInfo();
                    break;
                case CommandConstant.EXIT:
                    showExit();
                default:
                    showCommandError();
                    break;
            }
        }
    }

    private static void processArgument(final String[] arguments) {
        if (arguments == null || arguments.length == 0 || arguments[0] == null || arguments[0].isEmpty()) return;
        String argument = arguments[0];

        switch (argument) {
            case ArgumentConstant.ABOUT:
                showAbout();
                break;
            case ArgumentConstant.VERSION:
                showVersion();
                break;
            case ArgumentConstant.HELP:
                showArgumentHelp();
                break;
            case ArgumentConstant.INFO:
                showInfo();
                break;
            default:
                showArgumentError();
                break;
        }
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("email: aayakovlev@t1-consulting.ru");
        System.out.println("name: Yakovlev Anton.");
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.7.1");
    }

    private static void showArgumentHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - show developer info.\n", ArgumentConstant.ABOUT);
        System.out.printf("%s - show application version.\n", ArgumentConstant.VERSION);
        System.out.printf("%s - show arguments description.\n", ArgumentConstant.HELP);
        System.out.printf("%s - show hardware info.\n", ArgumentConstant.INFO);
    }
    private static void showCommandHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - show developer info.\n", CommandConstant.ABOUT);
        System.out.printf("%s - show application version.\n", CommandConstant.VERSION);
        System.out.printf("%s - show command description.\n", CommandConstant.HELP);
        System.out.printf("%s - show hardware info.\n", CommandConstant.INFO);
        System.out.printf("%s - exit program.\n", CommandConstant.EXIT);
    }

    private static void showInfo() {
        final Runtime runtime = Runtime.getRuntime();
        final long availableCores = runtime.availableProcessors();
        final long maxMemory = runtime.maxMemory();
        final long freeMemory = runtime.freeMemory();
        final long totalMemory = runtime.totalMemory();
        final long usedMemory = totalMemory - freeMemory;

        final boolean checkMaxMemory = maxMemory == Long.MAX_VALUE;
        final String maxMemoryFormat = checkMaxMemory ? "no limit" : FormatUtil.format(maxMemory);
        final String freeMemoryFormat = FormatUtil.format(freeMemory);
        final String totalMemoryFormat = FormatUtil.format(totalMemory);
        final String usedMemoryFormat = FormatUtil.format(usedMemory);

        System.out.println("[INFO]");
        System.out.println("Available cores: " + availableCores);
        System.out.println("Max memory: " + maxMemoryFormat);
        System.out.println("Total memory: " + totalMemoryFormat);
        System.out.println("Free memory: " + freeMemoryFormat);
        System.out.println("Used memory: " + usedMemoryFormat);
    }

    private static void showExit() {
        System.out.println("[EXIT]");
        System.exit(0);
    }

    private static void showArgumentError() {
        System.err.println("[ERROR]");
        System.err.println("Passed argument not recognized...");
        System.err.println("Try 'java -jar task-manager.jar -h' for more information.");
        System.exit(1);
    }

    private static void showCommandError() {
        System.err.println("[ERROR]");
        System.err.println("Passed command not recognized...");
        System.err.println("Try 'help' for more information.");
    }

}
